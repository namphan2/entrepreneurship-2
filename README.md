# Entrepreneurship-2

Project Enterprise Web Application 2018/2019

Team IS203-3 / Entrepreneurship Team 2

# Getting started

These instructions will help you set up the application.

### Prerequisites

Required software:

* Visual Studio Code (or any other similar editor/IDE)
* Intelij
* MySQL WorkBench (or any other similar program)

### Installing

**Run back-end server (Spring Boot)**:
1. Import the project into your IDE.
2. Navigate to the file application.properties (../src/main/resources/application.properties)
3. Change the database username and password to your own database login data
```
spring.datasource.url=jdbc:mysql://localhost:3307/entrepreneurship
spring.datasource.username=YOUR_USERNAME
spring.datasource.password=YOUR_PASSWORD
```
4. Check if the port number is correct and create an empty database schema with the name "entrepreneurship". Then uncomment the first line of application.properties when running the application for the first time:
```
spring.jpa.hibernate.ddl-auto=create
```
5. Run the project
6. Use the three SQL INSERT files in the map **docs-sql-files** to load important data into the database and the Admin account. (Pass: welcome)

**Run front-end server (Angular)**: 
1. Import the project into a Code Editor. (Visual Studio Code recommended)
2. Open a new terminal (in the code editor) and change directory to the root folder of the project.
```
cd /example/project/root
```
3. Build/Run the project by typing the following commands:

Installing all the **node_modules**

```
npm install
```

Next run the application

```
ng serve -o
```
In case of problems, please read the installation manual in the map **docs-sql-files**.
